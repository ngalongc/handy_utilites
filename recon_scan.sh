#!/bin/bash

ip_address=$(ifconfig | grep inet | grep -v "127.0.0.1" | grep -v "inet6"|cut -d ":" -f 2 | cut -d " " -f 1)
echo "IP address is $ip_address"
echo "Start Scanning the whole C network"
echo "nmap -sP $ip_address/24"
nmap -sP $ip_address/24

# $ip_address should be the ip address of the host if there is only one network adapter
# And automatically start reconning the whole C network
